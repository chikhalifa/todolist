-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 26, 2019 at 07:37 PM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `to-do`
--

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) DEFAULT NULL,
  `name` varchar(55) NOT NULL,
  `user` varchar(255) NOT NULL,
  `done` varchar(255) NOT NULL,
  `created` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `user`, `done`, `created`) VALUES
(0, 'Chika Joseph', '2', '0', '2019-07-14 00:00:25'),
(0, 'Chika Joseph', '2', '0', '2019-07-14 00:36:39'),
(0, 'PHP CODE', '2', '0', '2019-07-26 00:37:49'),
(NULL, 'esther metieh', '2', '0', '2019-07-26 00:45:52'),
(NULL, 'esther metieh', '2', '0', '2019-07-26 01:05:47');

-- --------------------------------------------------------

--
-- Table structure for table `list`
--

CREATE TABLE `list` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `user` varchar(50) NOT NULL,
  `done` varchar(50) NOT NULL,
  `created` varchar(50) NOT NULL,
  `trn_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `list`
--

INSERT INTO `list` (`id`, `name`, `user`, `done`, `created`, `trn_date`) VALUES
(1, 'esther metieh', '2', '0', '2019-07-26 00:49:22', '0000-00-00 00:00:00'),
(2, 'rrrrrrrrrrrrrrrrr', '2', '0', '2019-07-26 00:50:29', '0000-00-00 00:00:00'),
(3, 'PHP CODE', '2', '0', '2019-07-26 00:50:36', '0000-00-00 00:00:00'),
(4, 'esther', '2', '0', '2019-07-26 00:50:42', '0000-00-00 00:00:00'),
(5, 'esther metieh', '2', '0', '2019-07-26 00:57:48', '0000-00-00 00:00:00'),
(6, 'esther', '2', '0', '2019-07-26 00:57:53', '0000-00-00 00:00:00'),
(7, 'esther metieh', '2', '0', '2019-07-26 00:59:18', '0000-00-00 00:00:00'),
(8, 'PHP CODE', '2', '0', '2019-07-26 00:59:23', '0000-00-00 00:00:00'),
(9, 'esther metieh', '2', '0', '2019-07-26 01:00:33', '0000-00-00 00:00:00'),
(10, 'chika', '2', '0', '2019-07-26 01:04:15', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `list`
--
ALTER TABLE `list`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `list`
--
ALTER TABLE `list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
